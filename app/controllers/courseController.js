// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Course Model
const courseModel = require("../models/courseModel");

// Create course
const createCourse = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;

    // console.log(body);
  //B2: validate dữ liệu
  if (!body.courseName) {
    return res.status(400).json({
      message: 'courseCode is required!'
    })
  }
  console.log(body.courseCode);
  if (!body.courseName) {
    return res.status(400).json({
      message: 'courseName is required!'
    })
  }

  if (!Number.isInteger(body.price) || body.price < 0) {
    return res.status(400).json({
      message: 'price is invalid!'
    })
  }
  if (!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
    return res.status(400).json({
      message: 'discountPrice is invalid!'
    })
  }
  if (!body.duration) {
    return res.status(400).json({
      message: 'duration is required!'
    })
  }
  if (!body.level) {
    return res.status(400).json({
      message: 'level is required!'
    })
  }
  if (!body.coverImage) {
    return res.status(400).json({
      message: 'coverImage is required!'
    })
  }
  if (!body.teacherName) {
    return res.status(400).json({
      message: 'teacherName is required!'
    })
  }
  if (!body.teacherPhoto) {
    return res.status(400).json({
      message: 'teacherPhoto is required!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newCourseData = {
    _id: mongoose.Types.ObjectId(),
    courseCode: body.courseCode,
    courseName: body.courseName,
    price: body.price,
    discountPrice: body.discountPrice,
    duration: body.duration,
    level: body.level,
    coverImage: body.coverImage,
    teacherName: body.teacherName,
    teacherPhoto: body.teacherPhoto,
    isPopular: body.isPopular,
    isTrending: body.isTrending,
  }
  courseModel.create(newCourseData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newCourse: data
    })
  })

}

// Get all course 
const getAllCourse = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  courseModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Get all courses successfully",
      courses: data
    })
  })
}

// Get course by id
const getCourseById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let courseId = req.params.courseId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return res.status(400).json({
      message: "Course ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  courseModel.findById(courseId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Get course successfully",
      course: data
    })
  })
}

// Update course by id
const updateCourseById = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.courseId;
  let body = req.body;
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  const courseUpdate = {

  };
  if (body.courseCode) {
    courseUpdate.courseCode = body.courseCode;
  }
  if (body.courseName) {
    courseUpdate.courseName = body.courseName;
  }
  if (body.price) {
    courseUpdate.price = body.price;
  }
  if (body.discountPrice) {
    courseUpdate.discountPrice = body.discountPrice;
  }
  if (body.duration) {
    courseUpdate.duration = body.duration;
  }
  if (body.level) {
    courseUpdate.level = body.level;
  }
  if (body.coverImage) {
    courseUpdate.coverImage = body.coverImage;
  }
  if (body.teacherName) {
    courseUpdate.teacherName = body.teacherName;
  }
  if (body.teacherPhoto) {
    courseUpdate.teacherPhoto = body.teacherPhoto;
  }
  if (body.isPopular) {
    courseUpdate.isPopular = body.isPopular;
  }
  if (body.isTrending) {
    courseUpdate.isTrending = body.isTrending;
  }


  courseModel.findByIdAndUpdate(id, courseUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update course successfully",
      course: data
    })
  })

}

// Delete course by id
const deleteCourseById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let courseId = req.params.courseId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return res.status(400).json({
      message: "Course ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  courseModel.findByIdAndDelete(courseId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(204).json({
      message: "Delete course successfully"
    })
  })
}

// Export Course controller thành 1 module
module.exports = {
  createCourse,
  getAllCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById
}


