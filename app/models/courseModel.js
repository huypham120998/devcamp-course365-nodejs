// Import thư viên mongooseJs
const mongoose = require("mongoose");

// Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;

// Khai báo course schema
const courseSchema = new Schema({
    courseCode: {
        type: String,
        required: true,
        unique: true
    },
    courseName: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true,
    },
    level: {
        type: String,
        required: true,
    },
    coverImage: {
        type: String,
        required: true,
    },
    teacherName: {
        type: String,
        required: true,
    },
    teacherPhoto: {
        type: String,
        required: true,
    },
    isPopular: {
        type: Boolean,
        default: true,
    },
    isTrending: {
        type: Boolean,
        default: false,
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        }
    ]
}, {
    timestamps: true
});

module.exports = mongoose.model("Course", courseSchema);