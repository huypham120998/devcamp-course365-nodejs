//khai báo thư viện express
const express = require('express');
const { courseMiddleware } = require('../middlewares/courseMiddleware');
//tạo router
const courseRouter = express.Router();

const { 
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
} = require("../controllers/courseController");
//sủ dụng middle ware
courseRouter.use(courseMiddleware);

courseRouter.get("/courses", getAllCourse);

courseRouter.post("/courses", createCourse);

courseRouter.get("/courses/:courseId", getCourseById);

courseRouter.put("/courses/:courseId", updateCourseById);

courseRouter.delete("/courses/:courseId", deleteCourseById);

// Export dữ liệu thành 1 module
module.exports = {courseRouter};