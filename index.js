// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

//Import thư viện mongoose
const mongoose = require("mongoose");

const app = express();

const path = require('path');
const { courseRouter } = require('./app/routes/courseRouter');
//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))



const port = 8000;


mongoose.connect("mongodb://localhost:27017/CRUD_Course365", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});

//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(path.join(__dirname ,"/public")))


app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile( path.join(__dirname, './views/index.html'));
})
app.get("/list-courses", (request, response) => {
    console.log(__dirname);
    response.sendFile( path.join(__dirname, './views/listCourses.html'));
})
app.use('/', courseRouter);
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})