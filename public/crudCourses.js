$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gAllCourses = [];
  var gCoursesTable = "";
  var gCourseId = 0;
  var gStt = 0;
  var gNameCol = ["STT", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending"]; // gán với tên các cột trong array
  const gCOURSES_STT_COL = 0;
  const gCOURSES_CODE_COL = 1;
  const gCOURSES_NAME_COL = 2;
  const gPRICE_COL = 3;
  const gDISCOUNTPRICE_COL = 4;
  const gDURATION_COL = 5;
  const gLEVEL_COL = 6;
  const gCOVER_IMG_COL = 7;
  const gTEACHER_NAME_COL = 8;
  const gTEACHER_PHOTO_COL = 9;
  const gIS_POPULAR_COL = 10;
  const gIS_TRENDING_COL = 11;
  const gACTION_COL = 12;
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

    onPageLoading();

  $("#btn-add-courses").on("click", function () {
    onBtnAddNewCoursesClick();
  });
  $("#courses-table").on("click", ".btn-update", function () {
    onBtnUpdateCourseClick(this);
  });
  $("#courses-table").on("click", ".btn-delete", function () {
    onBtnDeleteCourseClick(this);
  });

  // gán sự kiện cho nút Create User (trên modal)
  $("#btn-create-course").on("click", function () {
    onBtnCreateCourseClick();
  });
  $("#btn-update-course").on("click", function () {
    onBtnModelUpdateCourseClick();
  });
  $("#btn-confirm-delete-user").on("click", function () {
    onBtnModelDeleteCourseClick();
  });






  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  /* add JSON data vào DataTable sử dụng add trực tiếp DataTable row */
  // input plugin

  // onPageLoading();
  function onPageLoading() {
    gStt = 0;
    callApiGetAllCourse();
    // $('#courses-table').dataTable();
     gCoursesTable = $("#courses-table").DataTable({
        destroy: true,
        data: gAllCourses,
        columns: [
          { data: gNameCol[gCOURSES_STT_COL] },
          { data: gNameCol[gCOURSES_CODE_COL] },
          { data: gNameCol[gCOURSES_NAME_COL] },
          { data: gNameCol[gPRICE_COL] },
          { data: gNameCol[gDISCOUNTPRICE_COL] },
          { data: gNameCol[gDURATION_COL] },
          { data: gNameCol[gLEVEL_COL] },
          { data: gNameCol[gCOVER_IMG_COL] },
          { data: gNameCol[gTEACHER_NAME_COL] },
          { data: gNameCol[gTEACHER_PHOTO_COL] },
          { data: gNameCol[gIS_POPULAR_COL] },
          { data: gNameCol[gIS_TRENDING_COL] },
          { data: gNameCol[gACTION_COL] },
        ],
        columnDefs: [           // định nghĩa các cột cần hiện ra
          {
            targets: gACTION_COL,      // cột action
            defaultContent: "<div class = 'row p-2'><button class='mb-2 btn btn-info btn-update'>Sửa</button> <button class='btn btn-danger btn-delete'>Xóa</button></div>",
            className: "text-left"
          },
          {
            targets: gCOVER_IMG_COL,
            render: function (data) {
              return '<img style = "width: 100px;" src="' + data + '">'
            }
          },
          {
            targets: gTEACHER_PHOTO_COL,
            render: function (data) {
              return '<img style = "width: 100px;" src="' + data + '">'
            }
          },
          {
            targets: gCOURSES_STT_COL,
            render: function (data) {
              return gStt++;
            }
          },
        ]
      });
    
  }
  // Hàm xử lý sự kiện khi nút Thêm mới đc click
  function onBtnAddNewCoursesClick() {
    // hiển thị modal trắng lên
    $("#create-courses-modal").modal("show");
  }
  // hàm xử lý sự kiện create voucher modal click
  function onBtnCreateCourseClick() {
    // khai báo đối tượng chứa voucher data
    var vCourseObj = {
      courseCode: "",
      courseName: "",
      price: 0,
      discountPrice: 0,
      duration: "",
      level: "",
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: true,
      isTrending: true
    };

    //B1: Thu thập dữ liệu
    getCreateCoursesData(vCourseObj);
    // B2: Validate insert
    if (validateCreateCourseData(vCourseObj)) {
      // B3: insert course
        callApiPostCourses(vCourseObj);

    //   gCoursesDB.courses.push(vCourseObj);

    //   loadDataToCourseTable(gCoursesDB.courses);
      // B4: xử lý front-end
      resetCreateCourseForm();
      $("#create-courses-modal").modal("hide");
      onPageLoading();
      // 
    }
  }
  function onBtnUpdateCourseClick(paramBtnUpdate) {
    // lưu thông tin voucherId đang được edit vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramBtnUpdate);
    // load dữ liệu vào các trường dữ liệu trong modal
    loadDataUpdateCourseToModel(paramBtnUpdate);
    $("#update-course-modal").modal("show");

   

  }
  function getCourseById(paramCourseId){
    loadDataUpdateCourseToModel(CourseResult);
  }
  function onBtnModelUpdateCourseClick() {
    var vCourseObj = {
      courseCode: "",
      courseName: "",
      price: 0,
      discountPrice: 0,
      duration: "",
      level: "",
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: true,
      isTrending: true
    };
    getDataCourseUpdateId(vCourseObj);
    if (validateUpdateCourseData(vCourseObj)) {
      
      callApiUpdateCourse(vCourseObj);
      onPageLoading();
      // B4: xử lý front-end
      resetUpdateCoursesForm();
      $("#update-course-modal").modal("hide");
    }
  }
  function onBtnDeleteCourseClick(paramBtnDelete) {
    // lưu thông tin voucherId đang được delete vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramBtnDelete);
    // hiển thị modal confirm lên
    $("#delete-confirm-modal").modal("show");
  }


  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  function callApiUpdateCourse(paramObject) {
    $.ajax({
        type: "PUT",
        async:false,
        url: "/courses/" + gCourseId,
        dataType: "json",
        data: JSON.stringify(paramObject),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
          alert("Update courses thành công");
        },
        error: function (error) {
            alert(error.responseText);
        },
    })
}


  function callApiDeleteCourse() {
    debugger;
    $.ajax({
        url: "/courses/" + gCourseId,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("DELETE thông tin thành công!");
            location.reload();
        },
        error: function (error) {
            alert(error.responseText);
        },
    })
}

  function callApiGetAllCourse(){
    $.ajax({
      url: "/courses",
      type: "GET",
      dataType: "json",
      async: false,
      success: function (responseJson) {
        console.log(responseJson);
        gAllCourses = responseJson.courses;
      },
      error: function (ajaxContent) {
        console.log(ajaxContent);
      }
    })
  }
  function callApiPostCourses(paramObject) {
    $.ajax({
        type: "POST",
        url: "/courses",
        data: JSON.stringify(paramObject),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (res) {
            alert("Thêm course thành công");
            console.log(res);
 
        },
        error: function (res) {
            console.log(res.status)
        }
    });
}

  function onBtnModelDeleteCourseClick() {
    debugger;
    callApiDeleteCourse();
    onPageLoading();
    $("#delete-confirm-modal").modal("hide");
  }
  function callApiDeleteUser() {
    $.ajax({
      url: "http://42.115.221.44:8080/crud-api/users/" + gUserId,
      type: 'DELETE',
      dataType: 'json',
      async: false,
      success: function (paramObjectUser) {
        console.log(paramObjectUser);

        alert("Xóa user có id " + gUserId + " thành công");
      }
    });
  }
  function resetUpdateCoursesForm() {
    $("#input-update-courses-code").val("");
    $("#input-update-courses-name").val("");
    $("#input-update-price").val("");
    $("#input-update-discount-price").val("");
    $("#input-update-duration").val("");
    $("#select-update-level").val("Beginner");
    $("#input-update-coverimg").val("");
    $("#input-update-teacher-name").val("");
    $("#input-update-teacher-photo").val("");
    $("#select-update-popular").val("true");
    $("#select-update-trending").val("true");
  }
  function CallApiUpdateUserById(paramUserObj) {
    $.ajax({
      async: false,
      url: "http://42.115.221.44:8080/crud-api/users/" + gUserId,
      type: 'PUT',
      data: JSON.stringify(paramUserObj),
      contentType: "application/json;charset=UTF-8",
      success: function (res) {
        gUsersArr = res;
        console.log(res);
        alert("Cập nhật user có id " + gUserId + " thành công");
      },
      error: function (error) {
        alert(error.responseText);
      }
    });
  }
  function getDataCourseUpdateId(paramCourseObj) {
    paramCourseObj.courseCode = $.trim($("#input-update-courses-code").val());
    paramCourseObj.courseName = $.trim($("#input-update-courses-name").val());
    paramCourseObj.price = $.trim($("#input-update-price").val());
    paramCourseObj.discountPrice = $("#input-update-discount-price").val();
    paramCourseObj.duration = $.trim($("#input-update-duration").val());
    paramCourseObj.level = $("#select-update-level").val();
    paramCourseObj.coverImage = $.trim($("#input-update-coverimg").val());
    paramCourseObj.teacherName = $("#input-update-teacher-name").val();
    paramCourseObj.teacherPhoto = $.trim($("#input-update-teacher-photo").val());
    paramCourseObj.isPopular = $("#select-update-popular").val();
    paramCourseObj.isTrending = $("#select-update-trending").val();
  }
  function CallApiGetUserById(paramUserId) {
    $.ajax({
      url: "http://42.115.221.44:8080/crud-api/users/" + paramUserId,
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function (paramObjectUser) {
        console.log(paramObjectUser);
        gUsersArr = paramObjectUser;
        loadDataUpdateUserToModel(gUsersArr);
      }
    });
  }
  function loadDataUpdateCourseToModel(paramCouseButton) {
    var vCurrentTr = $(paramCouseButton).closest("tr");
    var vTable = $("#courses-table").DataTable();
    var vDataRow = vTable.row(vCurrentTr).data();
    vCourseCode = vDataRow.courseCode;
    $("#input-update-courses-code").val(vDataRow.courseCode);
    $("#input-update-courses-name").val(vDataRow.courseName);
    $("#input-update-price").val(vDataRow.price);
    $("#input-update-discount-price").val(vDataRow.discountPrice);
    $("#input-update-duration").val(vDataRow.duration);
    $("#select-update-level").val(vDataRow.level);
    $("#input-update-coverimg").val(vDataRow.coverImage);
    $("#input-update-teacher-name").val(vDataRow.teacherName);
    $("#input-update-teacher-photo").val(vDataRow.teacherPhoto);
  }
  function getCourseIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vCourseData = gCoursesTable.row(vTableRow).data();
    debugger;
    return vCourseData._id;
  }

  function resetCreateCourseForm() {
    $("#input-create-courses-code").val("");
    $("#input-create-courses-name").val("");
    $("#input-create-price").val("");
    $("#input-create-discount-price").val("");
    $("#input-create-duration").val("");
    $("#select-level").val("Beginner");
    $("#input-create-coverimg").val("");
    $("#input-teacher-name").val("");
    $("#input-teacher-photo").val("");
    $("#select-popular").val("true");
    $("#select-trending").val("true");
  }
  function onBtnUpdateClick() {
    var vClosestRow = $(this).closest("tr");
    var vUserTable = $("#user-table").DataTable(); // tạo biến truy xuất đến data table
    var vDataRow = vUserTable.row(vClosestRow).data(); // lấy dữ liệu của dòng tương ứng
    console.log("Id user: " + vDataRow.id);
  }

  // hàm thu thập dữ liệu để create voucher
  function getCreateCoursesData(paramCourseObj) {
    paramCourseObj.courseCode = $.trim($("#input-create-courses-code").val());
    paramCourseObj.courseName = $.trim($("#input-create-courses-name").val());
    paramCourseObj.price = parseInt($.trim($("#input-create-price").val()));
    paramCourseObj.discountPrice = parseInt($("#input-create-discount-price").val());
    paramCourseObj.duration = $.trim($("#input-create-duration").val());
    paramCourseObj.level = $("#select-level").val();
    paramCourseObj.coverImage = $.trim($("#input-create-coverimg").val());
    paramCourseObj.teacherName = $("#input-teacher-name").val();
    paramCourseObj.teacherPhoto = $.trim($("#input-teacher-photo").val());
    paramCourseObj.isPopular = $("#select-popular").val();
    paramCourseObj.isTrending = $("#select-trending").val();
  }
  // hàm validate data
  function validateCreateCourseData(paramCourseObj) {
    if (paramCourseObj.courseCode === "") {
      alert("Bạn chưa nhập course code");
      return false;
    }
    if (paramCourseObj.courseName === "") {
      alert("Bạn chưa nhập course name");
      return false;
    }
    if (paramCourseObj.price === "") {
      alert("Bạn chưa nhập price");
      return false;
    }
    if (paramCourseObj.discountPrice === "") {
      alert("Bạn chưa nhập discount price");
      return false;
    }
    if (paramCourseObj.duration === "") {
      alert("Bạn chưa nhập duration");
      return false;
    }
    if (paramCourseObj.coverImage === "") {
      alert("Bạn chưa nhập Cover image");
      return false;
    }
    if (paramCourseObj.teacherName === "") {
      alert("Bạn chưa nhập teacher name");
      return false;
    }
    if (paramCourseObj.teacherPhoto === "") {
      alert("Bạn chưa nhập teacher Photo");
      return false;
    }
    return true;
  }
  function validateUpdateCourseData(paramCourseObj) {
    if (paramCourseObj.courseCode === "") {
      alert("Bạn chưa nhập course code");
      return false;
    }
    if (paramCourseObj.courseName === "") {
      alert("Bạn chưa nhập course name");
      return false;
    }
    if (paramCourseObj.price === "") {
      alert("Bạn chưa nhập price");
      return false;
    }
    if (paramCourseObj.discountPrice === "") {
      alert("Bạn chưa nhập discount price");
      return false;
    }
    if (paramCourseObj.duration === "") {
      alert("Bạn chưa nhập duration");
      return false;
    }
    if (paramCourseObj.coverImage === "") {
      alert("Bạn chưa nhập Cover image");
      return false;
    }
    if (paramCourseObj.teacherName === "") {
      alert("Bạn chưa nhập teacher name");
      return false;
    }
    if (paramCourseObj.teacherPhoto === "") {
      alert("Bạn chưa nhập teacher Photo");
      return false;
    }
    return true;
  }
  // hàm thực hiện insert voucher vào mảng
  function loadDataToCourseTable(paramCoursesArr) {
    gCoursesTable.clear();
    gCoursesTable.rows.add(paramCoursesArr);
    gCoursesTable.draw();
  }

});
