
  var gAllCourses = [];

$(document).ready(function () {
    onPageLoading();
  
  });
  function onPageLoading(){
    callApiGetAllCourse();
    getCoursePopular();
    getCourseTrending();
  }

  // call api
function callApiGetAllCourse() {
    $.ajax({
      url: "/courses",
      type: "GET",
      dataType: "json",
      async: false,
      success: function (responseJson) {
        console.log(responseJson);
        gAllCourses = responseJson.courses;
      },
      error: function (ajaxContent) {
        console.log(ajaxContent);
      }
    })
  }
  function getCoursePopular(){
    var vCoursesArr = [];
    vCoursesArr = gAllCourses.filter(function (paramCourses) {
         return (paramCourses.isPopular == true);
        });
        loadDataToCoursesPopular(vCoursesArr);
        
  }
  function getCourseTrending(){
    var vCoursesArr = [];
    vCoursesArr = gAllCourses.filter(function (paramCourses) {
         return (paramCourses.isTrending == true);
        });
        loadDataToCoursesTrending(vCoursesArr);
  }

  function loadDataToCoursesPopular(paramCoursesArr){
    var vCourseContainer = $("#popularCards");
    vCourseContainer.empty();
        let temp="";
        for(var bI = 0; bI < paramCoursesArr.length; bI++){
          temp+=`<div class="col-md-6 col-lg-3 mb-4">
                <div class="card">
                  <img src="`+ paramCoursesArr[bI].coverImage +`" class="card-img-top" alt="...">
                  <div class="card-body pb-1">
                    <p class="card-title text-primary font-weight-bold fs-3">`+ paramCoursesArr[bI].courseName +`
                    </p>
                    <div class="course-info">
                      <div class="time-course">
                        <span class="icon-time float-left mr-2">
                          <i class="fas fa-clock"></i>
                        </span>
                        <p class="float-left">`+ paramCoursesArr[bI].duration +`</p>
                      </div>
                      <div class="level-course float-left ml-2">
                        <p> `+ paramCoursesArr[bI].level+`</p>
                      </div>
                    </div>
                    <div class="course-price mt-1">
                      <p class="new-price float-left font-weight-bold">
                        `+ paramCoursesArr[bI].discountPrice+`
                      </p>
                      <s class="old-price ml-1 text-secondary">
                        `+ paramCoursesArr[bI].price+`
                      </s>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="teacher float-left">
                      <img class="rounded-circle float-left" src="`+paramCoursesArr[bI].teacherPhoto+`"
                      data-holder-rendered="true">
                      <p class="ml-5 small">`+ paramCoursesArr[bI].teacherName +`</p>
                    </div>
                    <div class="card-mark float-right">
                      <i class="text-secendary fas fa-bookmark"></i>
                    </div>
                    
                  </div>
                </div>
              </div>`;
        }
        vCourseContainer.html(temp);
  }
  function loadDataToCoursesTrending(paramCoursesArr){
    var vCourseContainer = $("#TrendingCards");
    vCourseContainer.empty();
        let temp="";
        for(var bI = 0; bI < paramCoursesArr.length; bI++){
          temp+=`<div class="col-md-6 col-lg-3 mb-4">
                <div class="card">
                  <img src="`+ paramCoursesArr[bI].coverImage +`" class="card-img-top" alt="...">
                  <div class="card-body pb-1">
                    <p class="card-title text-primary font-weight-bold fs-3">`+ paramCoursesArr[bI].courseName +`
                    </p>
                    <div class="course-info">
                      <div class="time-course">
                        <span class="icon-time float-left mr-2">
                          <i class="fas fa-clock"></i>
                        </span>
                        <p class="float-left">`+ paramCoursesArr[bI].duration +`</p>
                      </div>
                      <div class="level-course float-left ml-2">
                        <p> `+ paramCoursesArr[bI].level+`</p>
                      </div>
                    </div>
                    <div class="course-price mt-1">
                      <p class="new-price float-left font-weight-bold">
                        `+ paramCoursesArr[bI].discountPrice+`
                      </p>
                      <s class="old-price ml-1 text-secondary">
                        `+ paramCoursesArr[bI].price+`
                      </s>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="teacher float-left">
                      <img class="rounded-circle float-left" src="`+paramCoursesArr[bI].teacherPhoto+`"
                      data-holder-rendered="true">
                      <p class="ml-5 small">`+ paramCoursesArr[bI].teacherName +`</p>
                    </div>
                    <div class="card-mark float-right">
                      <i class="text-secendary fas fa-bookmark"></i>
                    </div>
                    
                  </div>
                </div>
              </div>`;
        }
        vCourseContainer.html(temp);
  }
//   showCoursesPopular();
//   showCoursesTrending();
//   function showCoursesPopular(){
//     var vCoursesPopularArr = [];
//     vCoursesPopularArr = gAllCourses.filter(function (paramCourses) {
//      return (paramCourses.isPopular == true);
//     });
//     console.log("Courses Popular: ");
//     console.log(vCoursesPopularArr);
//     var vPopularContainer = $("#popularCards");
//     vPopularContainer.empty();
//     let temp="";
//     for(var bI = 0; bI < vCoursesPopularArr.length; bI++){
//       temp+=`<div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
//             <div class="card">
//               <img src="`+ vCoursesPopularArr[bI].coverImage +`" class="card-img-top" alt="...">
//               <div class="card-body pb-1">
//                 <p class="card-title text-primary font-weight-bold fs-3">`+ vCoursesPopularArr[bI].courseName +`
//                 </p>
//                 <div class="course-info">
//                   <div class="time-course">
//                     <span class="icon-time float-left mr-2">
//                       <i class="fas fa-clock"></i>
//                     </span>
//                     <p class="float-left">`+ vCoursesPopularArr[bI].duration +`</p>
//                   </div>
//                   <div class="level-course float-left ml-2">
//                     <p> `+ vCoursesPopularArr[bI].level+`</p>
//                   </div>
//                 </div>
//                 <div class="course-price mt-1">
//                   <p class="new-price float-left font-weight-bold">
//                     `+ vCoursesPopularArr[bI].discountPrice+`
//                   </p>
//                   <s class="old-price ml-1 text-secondary">
//                     `+ vCoursesPopularArr[bI].price+`
//                   </s>
//                 </div>
//               </div>
//               <div class="card-footer">
//                 <div class="teacher float-left">
//                   <img class="rounded-circle float-left" src="`+vCoursesPopularArr[bI].teacherPhoto+`"
//                   data-holder-rendered="true">
//                   <p class="ml-5 small">`+ vCoursesPopularArr[bI].teacherName +`</p>
//                 </div>
//                 <div class="card-mark float-right">
//                   <i class="text-secendary fas fa-bookmark"></i>
//                 </div>
                
//               </div>
//             </div>
//           </div>`;
//     }
//     vPopularContainer.html(temp);
//   }
//   function showCoursesTrending(){
//     var vCoursesTrendingArr = [];
//     vCoursesTrendingArr = gAllCourses.filter(function (paramCourses) {
//      return (paramCourses.isTrending == true);
//     });
//     console.log("Courses Trending: ");
//     console.log(vCoursesTrendingArr);
//     var vTrendingContainer = $("#TrendingCards");
//     vTrendingContainer.empty();
//     let temp="";
//     for(var bI = 0; bI < vCoursesTrendingArr.length; bI++){
//       temp+=`<div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
//             <div class="card">
//               <img src="`+ vCoursesTrendingArr[bI].coverImage +`" class="card-img-top" alt="...">
//               <div class="card-body pb-1">
//                 <p class="card-title text-primary font-weight-bold fs-3">`+ vCoursesTrendingArr[bI].courseName +`
//                 </p>
//                 <div class="course-info">
//                   <div class="time-course">
//                     <span class="icon-time float-left mr-2">
//                       <i class="fas fa-clock"></i>
//                     </span>
//                     <p class="float-left">`+ vCoursesTrendingArr[bI].duration +`</p>
//                   </div>
//                   <div class="level-course float-left ml-2">
//                     <p> `+ vCoursesTrendingArr[bI].level+`</p>
//                   </div>
//                 </div>
//                 <div class="course-price mt-1">
//                   <p class="new-price float-left font-weight-bold">
//                     `+ vCoursesTrendingArr[bI].discountPrice+`
//                   </p>
//                   <s class="old-price ml-1 text-secondary">
//                     `+ vCoursesTrendingArr[bI].price+`
//                   </s>
//                 </div>
//               </div>
//               <div class="card-footer">
//                 <div class="teacher float-left">
//                   <img class="rounded-circle float-left" src="`+vCoursesTrendingArr[bI].teacherPhoto+`"
//                   data-holder-rendered="true">
//                   <p class="ml-5 small">`+ vCoursesTrendingArr[bI].teacherName +`</p>
//                 </div>
//                 <div class="card-mark float-right">
//                   <i class="text-secendary fas fa-bookmark"></i>
//                 </div>
                
//               </div>
//             </div>
//           </div>`;
//     }
//     vTrendingContainer.html(temp);

